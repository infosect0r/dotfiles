require("onedark").setup {
    style = 'darker',
    term_colors = true, -- Change terminal color as per the selected theme style
    code_style = {
        comments = 'italic',
        keywords = 'none',
        functions = 'bold',
        strings = 'none',
        variables = 'none'
    },
    colors = {
    	bg = "#161616"
    }, -- Override default colors
    highlights = {}, -- Override highlight groups

    lualine = {
        transparent = false, -- lualine center bar transparency
    },

}

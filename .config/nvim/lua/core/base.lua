vim.g.mapleader = ' '

vim.opt.relativenumber = true
vim.opt.ruler = true
vim.opt.cursorline = true
vim.opt.ttyfast = true
vim.opt.textwidth = 120
vim.opt.wrap = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = false
vim.opt.autoindent = true

-- Annoyances
vim.opt.backup = false
vim.opt.swapfile = false
vim.opt.undofile = false
vim.opt.compatible = false
